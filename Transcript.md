
# 1

Hello, we're group 3. Today we're going to talk about parking
management systems.


# 2

This is the agenda for today's topic. First, we'll go through
the preface, then dive into the `Procedure` and `Generations`
Last part, we'll talk about the `Conclusion`


# 3

Imagining you've been working for years and you saved some
money. 


# 4

What do you want to buy then?


# 5

This is the statistic from MOTC. As you can see, the percentage
of 100 people owning cars has been increasing over past a decade.


# 6

You know what, actually it has been increasing for more than 
three decades, which means that very possible you'll eventually
own a car. Then `parking` will be an essential part of your 
daily life. 


# 7

Now let's get into the first section, `Procedure`.


# 8

We divide the procedure into three parts:
- Registration
- Tracking
- Logout


# 9 - 11

> Read the slide.


# 12

Now let's get into `Generations`. We divided the generations
into three:
- Generation 1: Manual
- Generation 2: Semi-Automatic
- Generation 3: Automatic

We'll go through them one by one.


# 13

First, Manual. There are two main types of manual management:
- Street Side Parking
- Parking Lot/Garage

Usually, street-side parking ran by the local government. As
opposed to it, parking lots/garages are mostly run by the
private owner.


# 14

In manual management, all of them register the cars manually,
and the process is as simple as writing it down or even don't 
care about it. Some owners are tired of writing down, so 
they only charge the car once for the whole session.

The `Street Side Parking` will keep track of the cars that are
parked by the street, but others usually don't care about it.
They simply count how many spare spots there are by looking.

For logging out, many of them including `Street Side Parking`
will discard this part. Only if the parking time matters, 
then the logout will be performed to calculate the fee.


# 15

Here are the Pros and Cons of manual management.

> Read the slide.


# 16

Next, Semi-Automatic.

Nowadays, most of the parking facilities are semi-automatic
and it's the most diverse type among all three. Each procedure
can vary from one to another, so we split it up to explain
one by one. 


# 17

In regular semi-autos, the entry will have a barrier gate which
stops the cars from entering without registering to the system.


# 18

Two main tools are used to register the car, one is the `RFID` 
token, and the other is the `OCR`.

> Click

Prior one is a more traditional approach, there're quite a bit 
downsides:
- It's easy to lose the token, and it costs extra to compensate 
    for it.
- As the born nature of the `RFID`, the data can be erased
    due to the strong magnetic field.

This is where does `OCR` come in. The `OCR` is a centralized 
way, which means no tokens are distributed to the car owners. 
The system will scan the license plate and store the information 
in the database. This prevents the drawbacks we just mentioned.


# 19

Now is tracking. The major solutions on the market are counting,
infrared light, ultrasound and weight sensors. 


# 20

Counting is the simplest one. When registering a car, plus 
one, when logging out, minus one. It's a convenient way to 
track, but it's not the most reliable one. There exist 
multiple factors that can make lead to an inaccurate result.


# 21

Infrared light and ultrasound are working interchangeably.
They essentially measure the distance between the source and
the target. If the distance is over a certain threshold,
then the spot will be marked as empty and vice versa. 


# 22

The only problem is when the sensor gets obstructed, the 
result can be inaccurate.


# 23

Weight sensors are most expensive not only when installing,
but also when maintaining. But you can get the most accurate
reading from them. As opposed to the infrared and ultrasound,
they won't be triggered accidentally, since the threshold
is way higher.


# 24

Then logout. Before leaving, the car owner will have to pay
the parking fee. These days, most of the parking facilities 
are using automated payment machines, which accept coins and banknotes, even EasyCards as the payment method.

Some `OCR` based systems can also bind the license plate
with the credit card, so you can automate the payment
and leaving the facility with no hesitation.


# 25

Pros & Cons of semi-automatic are following.

> Read the slide.


# 26

Finally, Automatic. Automatic management is the most advanced
way to manage the parking.


# 27

General speaking, it consists of these parts:
- Auto Registration
- Auto Parking
- Auto Tracking
- Auto Logout


# 28

The main difference between semi and non-semi automatic is
that you don't need to park the car manually. The machine
will put the car in the corresponding spot automatically.
When you want to leave, the machine will bring it back to
the ground level.


# 29

Pros & Cons.

> Read the slide.


# 30

During the research, we found that this topic is actually
a very interesting one. It involves so many different
technologies and it's hard to summarize them all. So,
we only covered some of the most seen and used. If you
find it fascinating, you can always google it for more
information.


# 31

> Ending
